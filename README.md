---
tags: Julia
---

<center>
<img src='https://raw.githubusercontent.com/JuliaLang/julia-logo-graphics/master/images/julia-logo-color.svg'>

</center>


# **Julia**

# **Environment Setup**


> 1. Create a Dockerfile contain **Jupyter Notebook** and **Julia**
> 1. Using Docker **Dev Environment** to create a develop container
> 1. Using **Jupyter Notebook** in **VS Code**

## **1. Docker**
### `Dockerfile`
```yaml
# https://github.com/andferrari/julia_notebook/blob/master/Dockerfile

FROM "jupyter/minimal-notebook"

USER root

ENV JULIA_VERSION=1.8.4

RUN mkdir /opt/julia-${JULIA_VERSION} && \
    cd /tmp && \
    wget -q https://julialang-s3.julialang.org/bin/linux/x64/`echo ${JULIA_VERSION} | cut -d. -f 1,2`/julia-${JULIA_VERSION}-linux-x86_64.tar.gz && \
    tar xzf julia-${JULIA_VERSION}-linux-x86_64.tar.gz -C /opt/julia-${JULIA_VERSION} --strip-components=1 && \
    rm /tmp/julia-${JULIA_VERSION}-linux-x86_64.tar.gz

RUN ln -fs /opt/julia-*/bin/julia /usr/local/bin/julia

USER $NB_UID

# Add packages and precompile
RUN julia -e 'import Pkg; Pkg.update()' && \
    julia -e 'import Pkg; Pkg.add("Plots"); using Plots' && \
    julia -e 'import Pkg; Pkg.add("Distributions"); using Distributions' && \
    julia -e 'import Pkg; Pkg.add("Optim"); using Optim' && \  
    julia -e 'import Pkg; Pkg.add("FFTW"); using FFTW' && \  
    # julia -e 'import Pkg; Pkg.add("StatsPlots"); using StatsPlots' && \  
    julia -e 'import Pkg; Pkg.add("DSP"); using DSP' && \  
    julia -e 'import Pkg; Pkg.add("IJulia"); using IJulia' && \
    fix-permissions /home/$NB_USER
```
### `compose-dev.yaml`
```yaml
services:
  app:
    entrypoint:
    - sleep
    - infinity
    init: true
    volumes:
    - type: bind
      source: /var/run/docker.sock
      target: /var/run/docker.sock
    build: .
```

## **2. Dev Environment**
![](https://i.imgur.com/J5ulOZN.png)

![](https://i.imgur.com/lzd0wBB.png)

## **3. VS Code Extension**
![](https://i.imgur.com/wPeoBdz.png)

![](https://i.imgur.com/uBPY4zj.png)

> **📄 Reference**
> ---
> 1. [andferrari/julia_notebook/Dockerfile](https://github.com/andferrari/julia_notebook/blob/master/Dockerfile)